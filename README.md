# Really Simple Filters

A simple filtering system. Really, that's it.

## Getting Started

A really simple way to get started with Really Simple Filters

### Prerequisites

Currently the only dependency is jQuery, but a vanilla option will hopefully be present in the near future. Since the filters use `fadeIn()` and `fadeOut()` jQuery functions, they require the full jQuey build as opposed to the slim one. This is something which will hopefully be made an option in future, to allow use of jQuery slim when preferred.

### Installing

So let's get you set up!

The `dist/` folder contains both minified and unminified of the JS.

```
dist/
 |-- filters-jquery.js (~ 4KB)
 |-- filters-jquery.min.js (~ 2KB)
 -
```

Simply take the file you want and include it as you would with any other JS file!

No, really, that's it!

### Usage

Here's how you use the filters (psst, it's really simple)

#### Example

There is a fully-featured, single-page example of the filters in the `example/` folder. You're welcome!

#### Quick-Start Snippet
This is the quickest way to get started with the filters - copy the snippet below onto a page where you've included one of the JS files above and start clicking!

Note: `btn`, `btn-primary`, `btn-secondary` and `btn-danger` are [Bootstrap](https://www.getbootstrap.com) classes, but you can use your own for styling if you're styling in another way.
```
<div class="filter-container">
	<div class="filters">
		<span class="filter filter-default btn btn-secondary">Default</span>
		<span class="filter btn btn-primary" data-filter-val="option">Option</span>
		<span class="filter btn btn-danger" data-filter-val="choice">Choice</span>
		<span class="filter btn btn-success" data-filter-val="foo">Foo</span>
	</div>
	<div class="filtered-list">
		<div class="filtered-item" data-filter-val="option">Option</div>
		<div class="filtered-item" data-filter-val="choice">Choice</div>
		<div class="filtered-item" data-filter-val="foo">Foo</div>
		<div class="filtered-item" data-filter-val="option">Option</div>
		<div class="filtered-item" data-filter-val="option">Option</div>
		<div class="filtered-item" data-filter-val="foo">Foo</div>
		<div class="filtered-item" data-filter-val="choice">Choice</div>
		<div class="filtered-item" data-filter-val="option">Option</div>
		<div class="filtered-item" data-filter-val="foo">Foo</div>
		<div class="filtered-item" data-filter-val="choice">Choice</div>
		<div class="filtered-item" data-filter-val="choice">Choice</div>
	</div>
</div>
```

#### Explanation
A quick explanation of what's going on in the quick-start snippet.

**1.** The container has the class `filter-container` and is used to make sure that, if you're using multiple filtered
sections on a page, a set of filters doesn't get greedy and start changing other filter lists

**2.** The filters themselves (i.e. the things you click to change what shows) all have the class `filter` and a value for `data-filter-val` which dictates the 'value' of that filter (used when deciding which items to show/hide). These must also all sit within a containing div, but the class used (`filters` in the example) is entirely optional

**3.** The 'Default' filter has the class `filter-default`, which tells the filtering that if there are no filters seelcted, this is the one to fall back to. If the default class is omitted, then the fallback will be the first filter in the list. Clicking the default filter will disable all other filters

**4.** The list of items to filter are contained within a div that has the class `filtered-list` (which is required) and each have a `data-filter-val` value that matches the filter they apply to. *Note: allowing an item to belong to multiple filters is not yet supported*

Pretty simple, right?

Oh, and it's worth mentioning that filters (except any default) are not mutually exclusive. It's designed to allow multiple filters at once, so if you have the 'option' filter selected and then click the 'choice' filter, *both* will then be enabled.

## Built With

* [jQuery](https://code.jquery.com/)

## Authors

* **James Cushing** - *Creator* - [Ubiquity Code](https://www.ubiquitycode.co.uk)

## License

This project is licensed under the MIT License - see the [LICENCE.md](LICENCE.md) file for details
