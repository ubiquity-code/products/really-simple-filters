$(function(){

	// Apply default config
	filterConfig = Object.assign({
		fadeSpeed: 300,
		activeClass: 'filter-active',
		inactiveClass: 'filter-inactive'
	}, filterConfig );

	// Reset filters initially
	resetFilters( null );

	// Handle a filter click
	$( '.filter' ).on( 'click', function(){

		let $this			= $( this );										// The filter chosen
		let $container		= $this.closest( '.filter-container' );				// The current filter's container
		let $list			= getFilteredList( $this );							// The list to control
		let $filters		= $this.siblings( '.filter' ).addBack();			// All filters (including this one)
		let $activeFilters	= $filters.not( '.'+filterConfig.inactiveClass );	// Filters which are currently active
		let $defaultFilter	= $this.parent().children( '.filter-default' );		// The default ("all") filter

		let filterVal		= $this.data( 'filter-val' );							// The value to filter by
		let isDefault		= ( filterVal == $defaultFilter.data( 'filter-val' ) );	// Whether this filter is the default
		let enable			= $this.hasClass( filterConfig.inactiveClass );			// Whether to enable or disable the current filter
		let exclusive		= isDefault || $this.hasClass( 'filter-exclusive' );	// Whether this filter is an exclusive filter (is only active by itself)

		if( exclusive ){
			
			// Remove all filters first
			$none = removeFilters( $filters );

			// Add only this filter if enabling, only the default if disabling
			if( enable ){
				$activeFilters = addFilters( $this, $none );
			}else{
				$activeFilters = addFilters( $defaultFilter, $none );
			}

		}else{

			// Add or remove the appropriate filter
			if( enable ){
				$activeFilters = addFilters( $this, $activeFilters );
			}else{
				$activeFilters = removeFilters( $this, $activeFilters );
			}

		}

		// Apply filters if we have them, reset the filtering if we don't
		if( $activeFilters.length ){

			// Show and hide the right filtered items
			hideFilteredItems( $list, $filters.not( $activeFilters ), function(){
				showFilteredItems( $list, $activeFilters );
			});


		}else{
			resetFilters( $this.parent() );
		}

	});

	// Enable one or more filters
	function addFilters( $new, $existing ){

		// Default to empty set
		$existing = $existing || $( [] );

		$new.addClass( filterConfig.activeClass );
		$new.removeClass( filterConfig.inactiveClass );

		return $existing.add( $new );
	}

	// Disable one or more filters
	function removeFilters( $old, $existing ){

		// Default to empty set
		$existing = $existing || $( [] );

		$old.addClass( filterConfig.inactiveClass );
		$old.removeClass( filterConfig.activeClass );

		return $existing.not( $old );
	}

	// Get the list of items relevant to a filter
	function getFilteredList( $filter, needContainer ){

		// Default
		needContainer = needContainer || true;

		$container = needContainer ? $filter.closest( '.filter-container' ) : $filter;

		if( !$container.length ){
			$container = $( 'body' );
		}

		return $container.find( '.filtered-list' );

	}

	// Reset a filter set
	function resetFilters( $containers ){

		// Default filter container is body (for resetting all)
		if( !$containers ){
			$containers = $( 'body' ).find( '.filter-container' );
		}

		$containers.each(function( idx, el ){

			$container = $( el );

			// Which filters to make active/inactive
			$active		= $container.find( '.filter-default' );
			$filters	= $container.find( '.filter' );

			// Default to first filter if no default is present
			if( !$active.length && !$container.find( '.filter-default' ).length ){
				$active = $filters.first();
			}

			$inactive = $filters.not( $active );

			// Ge the list element
			$list = getFilteredList( $container, false );

			addFilters( $active );
			removeFilters( $inactive )

			//filterItems( $filters, $list );

			// Hide/show alerts
			showFilteredItems( $list );
		
		});

	}

	function filterItems( $filters, $list ){

		$filters.each(function( idx, el ){
			$filter = $( el );

			// "Show" logic
			if( $filter.hasClass( 'filter-default' ) ){
				$items = $list.find( '.filtered-item' );
			}else{
				$items = $list.find( '.filtered-item[data-filter-val='+$( el ).data( 'filter-val' )+']' );
			}

			$items.each(function( idx, el ){
				$item = $( el );
				
				// Show
				if( $filter.hasClass( filterConfig.activeClass ) && !$item.is( ':visible' ) ){
					$item.fadeIn( filterConfig.fadeSpeed );
				}else{

					// Hide
					if( $filter.hasClass( filterConfig.inactiveClass ) && $item.is( ':visible' ) ){
						$item.fadeOut( filterConfig.fadeSpeed );
					}

				}
			})
		});
	}

	// Show some items according to their filter(s)
	function showFilteredItems( $list, $filters, callback ){

		if( ( typeof( $filters ) !== 'undefined' ) && $filters.length ){

			$show = $( [] );

			// Items to show
			$filters.each(function( idx, el ){
				$filter = $( el );
				$new	= $filter.hasClass( 'filter-default' ) ? $list.find( '.filtered-item') : $list.find( '.filtered-item[data-filter-val='+$( el ).data( 'filter-val' )+']' );
			
				$new.each(function( idx, el ){
					$item = $( el );
					if( !$item.is( ':visible' ) ){
						$show	= $show.add( $new );
					}
				});
			});
			$show.fadeIn( filterConfig.fadeSpeed, callback );

		}else{
			$list.find( '.filtered-item' ).fadeIn( filterConfig.fadeSpeed, callback );
		}

	}

	// Hide some items according to their filter(s)
	function hideFilteredItems( $list, $filters, callback ){

		if( ( typeof( $filters ) !== 'undefined' ) && $filters.length ){

			$hide = $( [] );

			// Items to hide
			$filters.each(function( idx, el ){
				$old	= $list.find( '.filtered-item[data-filter-val='+$( el ).data( 'filter-val' )+']' );

				$old.each(function( idx, el ){
					$item = $( el );
					if( $item.is( ':visible' ) ){
						$hide = $hide.add( $old );
					}
				});
			});
			$hide.fadeOut( filterConfig.fadeSpeed, callback );

		}else{
			$list.find( '.filtered-item' ).fadeOut( filterConfig.fadeSpeed, callback );
		}
	}
});
